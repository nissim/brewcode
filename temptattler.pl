#!/usr/bin/perl -w
# temptattler.pl
# A perl script to check temperatures logged from a beer fermenter
# and send an alert email if the average temperature falls outside
# a given range.
#
# Example crontab (checking temperatures every hour):
#	0       *       *       *       *       /root/brewing/temptattler.pl
#
# See settings in "brew.cfg" for configuring this script.
#
# Originally by Nissim Chudnoff <nchudnoff@gmail.com>
#
# No rights reserved / public domain / CC0 Universal license.
use strict;
use Scalar::Util qw(looks_like_number);
use File::Basename;

# Get location of brew.cfg to load config to environment
my $ENVIRONMENT = dirname(__FILE__) . "/brew.cfg";
if (! -r $ENVIRONMENT) {
    print STDERR "Cannot read ${ENVIRONMENT}, please check config.\n";
    exit(1);
}
# This fine trickery is used to load the environment variables
# from a shell script into our env. It sources the file, then
# execs itself.
# http://www.perlmonks.org/bare/?node_id=291862
if (@ARGV && $ARGV [0] eq '--sourced_environment') {
    shift;
}
else {
    if (-f $ENVIRONMENT) {
        # Now we perform a double exec. The first exec gives us a shell,
        # allowing us the source the file with the environment variables.
        # Then, from within the shell we re-exec ourself - but with an
        # argument that will prevent us from going into infinite recursion.
        #
        # We cannot do a 'system "source $ENVIRONMENT"', because
        # environment variables are not propagated to the parent.
        #
        # Note the required trickery to do the appropriate shell quoting
        # when passing @ARGV back to ourselves.
        #
        # Also note that the program shouldn't normally be called with
        # '--sourced_environment' - if so, pick something else.
        @ARGV = map {s/'/'"'"'/g; "'$_'"} @ARGV;
        exec << "        --";
            set -a
            source '$ENVIRONMENT'
            exec    $0  --sourced_environment @ARGV;
        --
        die  "This should never happen.";
    }
}

# Read vars from Environment
my $csvFile = $ENV{'ROOTDIR'} . "/" . $ENV{'BREW'} . ".csv";
my $lineCnt = $ENV{'ALERT_READINGS'};

my $tempLow = $ENV{'MINTEMP'} - $ENV{'ALERT_UNDER'};
my $tempHigh = $ENV{'MAXTEMP'} + $ENV{'ALERT_OVER'};

my $emailFrom = $ENV{'ALERT_FROM'};
my $emailTo = $ENV{'ALERT_TO'};
my $emailSubj = $ENV{'ALERT_SUBJ'};
my $ssmtp = $ENV{'MAILER'};

my $sendMail = 0;
my $linecnt = 0;
my $avg = 0.0;

open TEMPS, "tail -${lineCnt} $csvFile |" or die "Can't read temperatures from $csvFile";
while(<TEMPS>) {
    if (looks_like_number($_)) {
        $avg += $_;
        $linecnt++;
    }
}
close TEMPS or die "Can't close $csvFile";

$avg /= $linecnt;

if ($avg < $tempLow) {
    $emailSubj = "${emailSubj} cold :(";
    $sendMail = 1;
}
elsif ($avg > $tempHigh) {
    $emailSubj = "${emailSubj} hawt :(";
    $sendMail = 1;
}
if ($sendMail) {
    open SSMTP_P, "|$ssmtp -f${emailFrom} $emailTo" or die "Can't pipe to $ssmtp";
    print SSMTP_P "From: $emailFrom\n";
    print SSMTP_P "To: $emailTo\n";
    print SSMTP_P "Subject: $emailSubj\n";
    print SSMTP_P "The fermenter temperature has gone out of range. Average temperature for the last ${linecnt} readings  is:\n";
    printf SSMTP_P "\t%.1f degrees F\n", $avg;
    print SSMTP_P "Temprature range should be $tempLow - ${tempHigh}.\n";
    close SSMTP_P or die "Can't close pipe to $ssmtp";
}

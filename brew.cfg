# Set values here for the particular batch being fermented.
# Go over all the settings once. After they are working,
# you should only need to change sections 1 and 2 for different
# brew fermentations.

####################################
### SECTION 1: Brewing variables ###
####################################
# BREW
# This variable sets the name of the RRD and CSV files
BREW=weizen

# MINTEMP and MAXTEMP
# Set the low/high ideal temperatures for brewing. They set where the lines
# on the graph will be drawn, as well as roughly setting when temperature
# alerts will be sent (see ALERT_OVER and ALERT_UNDER in section 4)
MINTEMP=68
MAXTEMP=72


#################################
### SECTION 2: Graph settings ###
#################################
# GRAPH_TITLE
# Title of the graph
GRAPH_TITLE="Temp (F) of fermentation vessel (${BREW})"

# GRAPH_W and GRAPH_H
# Graph dimensions, in pixels
GRAPH_W=1000
GRAPH_H=500

# GRAPH_UL and GRAPH_LL
# Upper and lower limits for graph output. Ignored if temperatures leave the
# range, so they aren't hard limits.
GRAPH_UL=75
GRAPH_LL=65

# GRAPH_LINE_NAME
# The name of the temperature line to be plotted
GRAPH_LINE_NAME=tFermenter

# GRAPH_LINE_COLOR
# The color (as a hex triplet) of the temperature line to plot
GRAPH_LINE_COLOR=C0C0C0

# GRAPH_MIN_COLOR and GRAPH_MAX_COLOR
# The color (as a hex triplet) to plot the min/max fermentation temperature
# lines
GRAPH_MIN_COLOR=0000FF
GRAPH_MAX_COLOR=FF0000

# GRAPH_FILE_PREFIX
# The name of the PNG files to output graphs to. They will be have suffixes
# _24hrs, _48hrs and _1wk appended.
GRAPH_FILE_PREFIX=fermenter


###########################################
### SECTION 3: Data collection settings ###
###########################################
# RRD_DS
# Data Source name to log in RRD
RRD_DS=beer

# RRD_STEP
# How often, in seconds, to expect temperature readings
RRD_STEP=300

# RRD_ARCROWS
# How many datapoints to save in the RRD file. A multiple of RRD_STEP.
# Eg, 25290 * 300s = 90 days.
RRD_ARCROWS=25290

# RRD_UNK
# How many seconds w/ no temperature before considering data point unknown
RRD_UNK=900

# RRD_TMIN and RRD_TMAX
# Minimum and maximum temperature values to store
RRD_TMIN=32
RRD_TMAX=120


#################################
### SECTION 4: Alert settings ###
#################################
# ALERT_OVER and ALERT_UNDER
# Sets the amount the average temperature must go over or under the MAXTEMP
# or MINTEMP values respectively before raising an alert
ALERT_OVER=1
ALERT_UNDER=2

# ALERT_READINGS
# How many lines to read from CSV to calculate average temperature
ALERT_READINGS=12

# ALERT_FROM
# The From address for the alert to appear from
ALERT_FROM="monitor@brewery.com"

# ALERT_TO
# Comma seperated list of addresses to send alerts to
ALERT_TO="brewer@brewery.com"

# ALERT_SUBJ
# Alert email subject line. It will be appended with "hot" or "cold" depending
# on which alert level is reached
ALERT_SUBJ="ALERT: The fermenter is too"

# MAILER
# Path to ssmtp/sendmail mailer program
MAILER="/usr/bin/ssmtp"

##################################
### SECTION 5: System settings ###
##################################
# ROOTDIR
# Sets the directory where RRD and CSV files are saved
ROOTDIR=/home/brewing

# TEMPSRC
# This script will be executed to read temperature. It should output a single
# reading, in F on stdout. Examples of this include Temper/KeyTemper for cheap
# USB temperature sensors.
TEMPSRC="python2 ${ROOTDIR}/temper.py"

# GRAPHDIR
# Sets the output directory for graphs
GRAPHDIR=${ROOTDIR}/http

# WEB_HOST
# Where to scp the contents of GRAPHDIR after graphing
WEB_HOST=apache@webserver.com:/home/http



#!/bin/bash
# tempgrapher.sh
# A script to make pretty graphs using RRDtool. Graphs are generated
# for the last 24 hours, 48 hours and week. The ideal fermentation
# range is delineated with horizontal lines.
# In addition, the script will use 'scp' to copy the graphs to
# another host (say, a webserver). To use this functionality you
# should set up ssh keys on both so the file copy does not prompt
# for a password.
#
# Example crontab (graphing every 2 hours):
#	0       */2     *       *       *       /root/brewing/tempgrapher.sh
#
# See settings in "brew.cfg" for configuring this script.
#
# No rights reserved / public domain / CC0 Universal license.
BASEDIR=$(dirname $BASH_SOURCE)
CFG=$BASEDIR/brew.cfg
if [ ! -r $CFG ]
then
    echo "Cannot read $CFG, please check config." >&2
    exit 1
fi
source $CFG
        
RRDFILE=${ROOTDIR}/${BREW}.rrd

cd ${ROOTDIR}

rrdtool graph ${GRAPHDIR}/${GRAPH_FILE_PREFIX}_24hrs.png --title "${GRAPH_TITLE}, 24hr" \
	 --start now-1d --end now --right-axis 1:0 \
	--width=${GRAPH_W} --height=${GRAPH_H} --step=${RRD_STEP} -v degreesF -u ${GRAPH_UL} -l ${GRAPH_LL} \
	DEF:temp1=${RRDFILE}:${RRD_DS}:AVERAGE \
	LINE2:temp1#${GRAPH_LINE_COLOR}:${GRAPH_LINE_NAME} \
        HRULE:${MINTEMP}#${GRAPH_MIN_COLOR}:tMin HRULE:${MAXTEMP}#${GRAPH_MAX_COLOR}:tMax

rrdtool graph ${GRAPHDIR}/${GRAPH_FILE_PREFIX}_48hrs.png --title "${GRAPH_TITLE}, 48hr" \
	--start now-2d --end now --right-axis 1:0 \
	--width=${GRAPH_W} --height=${GRAPH_H} --step=${RRD_STEP} -v degreesF -u ${GRAPH_UL} -l ${GRAPH_LL} \
	DEF:temp1=${RRDFILE}:${RRD_DS}:AVERAGE \
	LINE2:temp1#${GRAPH_LINE_COLOR}:${GRAPH_LINE_NAME} \
        HRULE:${MINTEMP}#${GRAPH_MIN_COLOR}:tMin HRULE:${MAXTEMP}#${GRAPH_MAX_COLOR}:tMax

rrdtool graph ${GRAPHDIR}/${GRAPH_FILE_PREFIX}_1wk.png --title "${GRAPH_TITLE}, 1 week" \
	--start now-7d --end now --right-axis 1:0 \
	--width=${GRAPH_W} --height=${GRAPH_H} --step=${RRD_STEP} -v degreesF -u ${GRAPH_UL} -l ${GRAPH_LL} \
	DEF:temp1=${RRDFILE}:${RRD_DS}:AVERAGE \
	LINE2:temp1#${GRAPH_LINE_COLOR}:${GRAPH_LINE_NAME} \
        HRULE:${MINTEMP}#${GRAPH_MIN_COLOR}:tMin HRULE:${MAXTEMP}#${GRAPH_MAX_COLOR}:tMax

scp -q ${GRAPHDIR}/* ${WEB_HOST}

#!/bin/bash
# templogger.sh
# A script to log temperatures (say from a beer fermentation vessel)
# to a RRD file. If the RRD file is not found, it will create it.
# Temperatures are also logged to a CSV file for easy human reading,
# as well as for "temptattler.pl" to monitor and send alerts.
#
# Example crontab (logging temperature every 5 minutes):
# 	*/5     *       *       *       *       /root/brewing/templogger.sh
#
# See settings in "brew.cfg" for configuring this script.
#
# Originally by Nissim Chudnoff <nchudnoff@gmail.com>
#
# No rights reserved / public domain / CC0 Universal license.
BASEDIR=$(dirname $BASH_SOURCE)
CFG=$BASEDIR/brew.cfg
if [ ! -r $CFG ]
then
    echo "Cannot read $CFG, please check config." >&2
    exit 1
fi
source $CFG

RRDFILE=${ROOTDIR}/${BREW}.rrd

BEER_F=`${TEMPSRC}`

# Check for RRD file, if not found create
if [ ! -w ${RRDFILE} ]
then
    rrdtool create ${RRDFILE} --step ${RRD_STEP} \
        DS:${RRD_DS}:GAUGE:${RRD_UNK}:${RRD_TMIN}:${RRD_TMAX} \
        RRA:AVERAGE:0:1:${RRD_ARCROWS}
fi

# Write to RRD
rrdtool update ${RRDFILE} -t ${RRD_DS} N@${BEER_F}

# write to csv file
echo ${BEER_F} >> ${ROOTDIR}/${BREW}.csv

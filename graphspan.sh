#!/bin/sh
# graphspan.sh
# Helper script to easily graph the entire length of the current brew. Reads
# most options from brew.cfg.
#
# See settings in "brew.cfg" for configuring this script.
#
# Originally by Nissim Chudnoff <nchudnoff@gmail.com>
#
# No rights reserved / public domain / CC0 Universal license.
BASEDIR=$(dirname $BASH_SOURCE)
CFG=$BASEDIR/brew.cfg
if [ ! -r $CFG ]
then
    echo "Cannot read $CFG, please check config." >&2
    exit 1
fi
source $CFG
RRDFILE=${ROOTDIR}/${BREW}.rrd

if [ "$#" -ne 2 ] && [ "$#" -ne 4 ]
then
    echo "Usage: $0 startdate enddate [width] [height]" >&2
    echo "    Date expressions should be of the form YYYYMMDD or any form understood by rrdtool." >&2
    exit 2
fi
if [ "$#" -eq 4 ]
then
    GRAPH_W=$3
    GRAPH_H=$4
fi

rrdtool graph ${BREW}_all.png --title "${GRAPH_TITLE} ${1}-${2}" --start $1 --end $2 \
    --right-axis 1:0 --width ${GRAPH_W} --height ${GRAPH_H} --step ${RRD_STEP} -v degreesF \
    DEF:temp1=${RRDFILE}:${RRD_DS}:AVERAGE \
    LINE2:temp1#${GRAPH_LINE_COLOR}:${GRAPH_LINE_NAME} \
    HRULE:${MINTEMP}#${GRAPH_MIN_COLOR}:tMin HRULE:${MAXTEMP}#${GRAPH_MAX_COLOR}:tMax
